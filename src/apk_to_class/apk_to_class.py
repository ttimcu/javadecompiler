import sys
import os
import zipfile

from shutil import copyfile
from subprocess import *


if len(sys.argv) == 1:
    sys.exit("You have to specify an .apk file.")
else:
    apkFileName = sys.argv[1]

    if not apkFileName.endswith(".apk"):
        sys.exit("The file you specified is not an .apk file")
    else:
        currentDirectory = os.path.dirname(os.path.realpath(__file__))

        workDirectory = currentDirectory + "\\" + apkFileName.split(".", -1)[0]

        if os.path.exists(workDirectory):
            directoryNr = 1
            workDirectory += str(directoryNr)

            while os.path.exists(workDirectory):
                stringDirectoryNr = str(directoryNr)
                workDirectory = workDirectory[:(-1 * len(stringDirectoryNr))] + stringDirectoryNr
                directoryNr += 1

        os.makedirs(workDirectory)
        workApkFile = workDirectory + "\\" + apkFileName
        copyfile(currentDirectory + "\\" + apkFileName, workApkFile)

        workApkZipFile = os.path.splitext(workApkFile)[0] + ".zip"

        os.rename(workApkFile, workApkZipFile)

        workDirectoryZippedFilesDirectory = workDirectory + "\\unzipped-apk"

        os.makedirs(workDirectoryZippedFilesDirectory)

        zip_ref = zipfile.ZipFile(workApkZipFile, 'r')
        zip_ref.extractall(workDirectoryZippedFilesDirectory)
        zip_ref.close()

        for filename in os.listdir(workDirectoryZippedFilesDirectory):
            if filename.endswith(".dex"):
                p = Popen(['dex2jar\\d2j-dex2jar.bat', workDirectoryZippedFilesDirectory + '\\' + filename])
                # output, errors = p.communicate()
                p.wait()

                filenameWithoutExtension = os.path.splitext(filename)[0]

                classesZipFile = workDirectory + "\\" + filenameWithoutExtension + ".zip"

                os.rename(currentDirectory + "\\" + filenameWithoutExtension + "-dex2jar.jar", classesZipFile)

                classFilesDirectory = workDirectory + "\\" + filenameWithoutExtension
                os.makedirs(classFilesDirectory)

                zip_ref = zipfile.ZipFile(classesZipFile, 'r')
                zip_ref.extractall(classFilesDirectory)
                zip_ref.close()

                print "Your .class files are in " + classFilesDirectory
